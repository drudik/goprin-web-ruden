import React from 'react';

import GooglePlay from '../../assets/img/theme/Google-Play.png';
import AppStore from '../../assets/img/theme/App-Store.png';

import { Col, Container, Row, Image, Button } from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    return (
        <Container className={"bg-footer-container text-white p-0"} fluid>
            <Container className={"footer-link-style"}>
                <Row className={"footer-middle-margin"}>
                    <Col className={"text-center footer-top-margin"}>
                        <h1 className={"footer-title"}>Завантаж додаток<br/>GoPrint - це безкоштовно!</h1>
                        <span className={"button-border-white"}>
                            <Button className={"footer-btn text-uppercase"}>завантажити</Button>
                        </span>
                    </Col>
                </Row>
                <Row className={"footer-bottom-margin"}>
                    <Col sm={3} className={"footer-column-font"}> 
                        <p><b>Офіс GoPrint</b></p>
                        <br/>
                        <a href="tel:+38 063 66 009">+38 063 66 0099</a>
                        <br/>
                        <a href="tel:+38 063 25 366 66">+38 063 25 366 66</a>
                        <br/>
                        <a href="mailto:goprint.ukraine@gmail.com">goprint.ukraine@gmail.com</a>
                        <br/><br/>
                        <p>м. Київ
                        <br/>
                        <a href="https://goo.gl/maps/kdDDVrCMwcKF7Mr29" target="_blank" rel="noreferrer">Вул. Казимира Малевича 86-П</a>
                        </p>
                    </Col>
                    <Col sm={3} className={"footer-navmenu"}>
                        <ul>
                            <li><a href="/#">Як користуватись</a></li>
                            <li><a href="/#">Переваги</a></li>
                            <li><a href="/#">Запитання</a></li>
                            <li><a href="/#">Додаток</a></li>
                        </ul>
                    </Col>
                    <Col sm={3} className={"footer-navmenu"}>
                        <ul>
                            <li><a href="/#">Політика конфіденційності</a></li>
                            <li><a href="/#">Публічна оферта</a></li>
                        </ul>
                    </Col>
                    <Col sm={3} className={"footer-images text-center"}>
                        <p><b>Встановлюйте наш додаток</b></p>
                        <br/>
                        <a href="/#">
                            <Image className={"footer-app-img"} src={GooglePlay} alt={'google_paly_store_link'}/>
                        </a>
                        <br/>
                        <a href="/#">
                            <Image className={"footer-app-img"} src={AppStore} alt={'io_app_store_link'}/>
                        </a>
                    </Col>
                </Row>
            </Container>
            <Container className={"bg-white text-dark text-center copywrite-container"} fluid>
                <Row>
                    <Col>
                        <p>2020 © Сервис автономной печати «GoPrint»</p>
                    </Col>
                </Row>
            </Container>
        </Container>
    );
}