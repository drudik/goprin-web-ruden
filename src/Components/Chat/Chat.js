import React, { useState } from 'react';

import icon_Insta from '../../assets/img/icons/instagram.png';
import icon_FB from '../../assets/img/icons/facebook.png';
import icon_Telega from '../../assets/img/icons/telegram.png';
import go_logo from '../../assets/img/ill/caht_logo_goprint.svg'
import heart_fill from '../../assets/img/ill/heart_fill_red.svg';
import heart_stroke from '../../assets/img/ill/heart_stroke.svg';

import { Row, Col, Container } from 'react-bootstrap';


// eslint-disable-next-line
export default () => {
     
    return(
        <Container className={"bg-white"} fluid>
            <Row>
                <Col className={"d-flex justify-content-center"}>
                    <div className={"chat-main-frame"}>
                        <div className={"chat-top-frame d-flex align-items-center"}>
                            <img src={go_logo} alt={'chat-logo'}/>
                            <p className={"chat-frame-title p-0 m-0"}>goprint.ukraine</p>
                        </div>
                        <div className={"chat-message-in-frame "}>
                            <ul id="scroll-4at">
                                <li className={"user-message"}>
                                    <span className={'heart-like-fill'}><img src={heart_fill} alt={"heart_fill"}/></span>
                                    <b>@ivanlypkan @goprint.ukraine</b>   
                                    <br/>
                                    <p>Неймовірно крута ідея, я трішки вражений, що подібне ноу-хау потрапило до мого міста. Буду постійним клієнтом)</p>
                                </li>
                                
                                <li className={"admin-message"}>
                                    <span className={"heart-like"}><img src={heart_stroke} alt={"heart_stroke"}/></span>
                                    <b>@goprint.ukraine @ivanlypkan</b>    
                                    <br/>
                                    <p>дякуємо за відгук приємного другу</p>
                                </li>

                                <li className={"user-message"}>
                                    <span className={'heart-like-fill'}><img src={heart_fill} alt={"heart_fill"}/></span>
                                    <b>@ivanlypkan @goprint.ukraine</b>   
                                    <br/>
                                    <p>Неймовірно крута ідея, я трішки вражений, що подібне ноу-хау потрапило до мого міста. Буду постійним клієнтом)</p>
                                </li>
                                
                                <li className={"admin-message"}>
                                    <span className={"heart-like"}><img src={heart_stroke} alt={"heart_stroke"}/></span>
                                    <b>@goprint.ukraine @ivanlypkan</b>    
                                    <br/>
                                    <p>дякуємо за відгук приємного другу</p>
                                </li>

                                <li className={"user-message"}>
                                    <span className={'heart-like-fill'}><img src={heart_fill} alt={"heart_fill"}/></span>
                                    <b>@ivanlypkan @goprint.ukraine</b>   
                                    <br/>
                                    <p>Неймовірно крута ідея, я трішки вражений, що подібне ноу-хау потрапило до мого міста. Буду постійним клієнтом)</p>
                                </li>
                                
                                <li className={"admin-message"}>
                                    <span className={"heart-like"}><img src={heart_stroke} alt={"heart_stroke"}/></span>
                                    <b>@goprint.ukraine @ivanlypkan</b>    
                                    <br/>
                                    <p>дякуємо за відгук приємного другу</p>
                                </li>
                                <li className={"user-message"}>
                                    <span className={'heart-like-fill'}><img src={heart_fill} alt={"heart_fill"}/></span>
                                    <b>@ivanlypkan @goprint.ukraine</b>   
                                    <br/>
                                    <p>Неймовірно крута ідея, я трішки вражений, що подібне ноу-хау потрапило до мого міста. Буду постійним клієнтом)</p>
                                </li>
                                
                                <li className={"admin-message"}>
                                    <span className={"heart-like"}><img src={heart_stroke} alt={"heart_stroke"}/></span>
                                    <b>@goprint.ukraine @ivanlypkan</b>    
                                    <br/>
                                    <p>дякуємо за відгук приємного другу</p>
                                </li>
                                <li className={"user-message"}>
                                    <span className={'heart-like-fill'}><img src={heart_fill} alt={"heart_fill"}/></span>
                                    <b>@ivanlypkan @goprint.ukraine</b>   
                                    <br/>
                                    <p>Неймовірно крута ідея, я трішки вражений, що подібне ноу-хау потрапило до мого міста. Буду постійним клієнтом)</p>
                                </li>
                                
                                <li className={"admin-message"}>
                                    <span className={"heart-like"}><img src={heart_stroke} alt={"heart_stroke"}/></span>
                                    <b>@goprint.ukraine @ivanlypkan</b>    
                                    <br/>
                                    <p>дякуємо за відгук приємного другу</p>
                                </li>
                            </ul>
                        </div>
                        <Row>
                            <Col className={"d-flex justify-content-start title-style-sm"}>
                                <div>
                                    <p className={"m-0 p-0"}>
                                        <span className={"text-uppercase"}>Читай та залишай відгуки!</span>
                                        <br/>
                                        <span>Дізнавайся про акції та новини!</span>
                                    </p> 
                                </div>
                                <div className={"chat-social-icon d-flex align-self-center"}>
                                    <a href="#top"><img src={icon_Telega} alt={"icon_telegram"} width={"32"}/></a>
                                    <a href="#top"><img src={icon_FB} alt={"icon_facebook"} width={"32"}/></a>
                                    <a href="#top"><img src={icon_Insta} alt={"icon_instagram"} width={"32"}/></a>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}