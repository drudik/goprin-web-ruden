import React from 'react';

import logo from '../../assets/img/brand/logo1.png';
import Location_pic from '../../assets/img/ill/location_pic.svg';

import { Button, Container, Navbar, NavDropdown, Nav, Image } from 'react-bootstrap';

function NavBar() {
    return (
        <Container className="top-nav fixed-top" fluid>
            <Navbar collapseOnSelect className="container" expand="lg" variant="dark">
            <Navbar.Brand className="text-logo logo" href="#home"><img src={logo} alt={'logo'}/></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="m-auto text-uppercase">
                <Nav.Link href="#features" className={"nav-text"}>Як користуватись</Nav.Link>
                <Nav.Link href="#FAQ" className={"nav-text"}>Запитання</Nav.Link>
                <Nav.Link href="#application" className={"nav-text"}>Додаток</Nav.Link>
                </Nav>
                <Nav>
                <Image src={Location_pic} alt={"localization"} style={{width: "26px"}}/>
                <Button className={"nav-login-btn text-uppercase"}>
                    Вхід
                </Button>
                <NavDropdown className={""} title="укр" id="collasible-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Укр</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Рус</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Eng</NavDropdown.Item>
                </NavDropdown>
                </Nav>
            </Navbar.Collapse>
            </Navbar>
        </Container>
    );
}

export default NavBar

