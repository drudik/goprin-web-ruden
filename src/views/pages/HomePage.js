// eslint-disable-next-line
import React from 'react';

import NavBar from '../../Components/NavBar/NavBar'; 
import FullBaner from '../partials/FullBaner';
import Steps from '../partials/Steps';
import SectionApp from '../partials/SectionApp';
import Faq from '../partials/Faq';
import ActionBtn from '../partials/ActionBtn'
import Partners from '../partials/Partners';
import Chat from '../../Components/Chat/Chat';
import Footer from '../../Components/Footer/Footer'
import VideoBlock from '../partials/VideoBlock';
import ChoiceApp from '../modal/ChoiceApp'

function HomePage() {
  return (
    <div>
      {/* NavBar - dynamic, reusable*/}
      <NavBar/> 
      <FullBaner/>
      <Steps/>
      <SectionApp/>
      <VideoBlock/>
      <Faq/>
      <ActionBtn/>
      <Partners/>
      <Chat/>
      <Footer/>
      <ChoiceApp/>
      {/* Footer - reuseable */}
    </div>
  );

}

export default HomePage;

