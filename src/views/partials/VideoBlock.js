import { React } from 'react';

import mp4 from '../../assets/media/GoPrint.mp4'
import ogv from '../../assets/media/GoPrint.ogv'
import swf from '../../assets/media/GoPrint.swf'
import webm from '../../assets/media/GoPrint.webm'
import Poster from '../../assets/img/theme/video_poster_1280.jpg'

import { Container } from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    
    return (
        <Container className={'p-0'} fluid>
            <Container className={"position-relative mt-5"}>
                <video controls width="100%" height="auto" muted poster={Poster}>
                    {/* <!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 --> */}
                    <source src={mp4} type="video/mp4" /> 
                    {/* <!-- WebM/VP8 для Firefox4, Opera, и Chrome --> */}
                    <source src={webm} type="video/webm"/> 
                    {/* <!-- Ogg/Vorbis для старых версий браузеров Firefox и Opera --> */}
                    <source src={ogv} type="video/ogg"/> 
                    {/* <!-- добавляем видеоконтент для устаревших браузеров, в которых нет поддержки элемента video --> */}
                    <object data={swf} type="application/x-shockwave-flash">
                        <param name="movie" value="video.swf"/>
                    </object>
                </video>
            </Container>
        </Container>
    );
}