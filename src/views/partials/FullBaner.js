import React from 'react';

import icon_Insta from '../../assets/img/icons/instagram.png';
import icon_FB from '../../assets/img/icons/facebook.png';
import icon_Telega from '../../assets/img/icons/telegram.png';

import { Container, Button, Col, Row } from 'react-bootstrap';

function FullBaner() {        
    return (
        <Container className={"position-relative p-0"} style={{marginTop: "85px"}} fluid>    
            <div className={"position-absolute p-0 banner-container-style"} style={{height: "500"}}/>
            <Container className={"banner-content"}>
                <Row>
                    <Col sm={10} >
                        <h1 className={"banner-text mt-5 banner-title text-uppercase font-roboto-black"}>мережа терміналів швидкого еко друку</h1>
                    </Col>
                    <Col sm={2} className={"d-flex flex-column align-items-end mt-5 "}>
                        <a href="#top"><img src={icon_Insta} alt={"icon_instagram"} width={"32"}/></a>
                        <a href="#top"><img src={icon_FB} alt={"icon_facebook"} width={"32"}/></a>
                        <a href="#top"><img src={icon_Telega} alt={"icon_telegram"} width={"32"}/></a>
                    </Col>
                </Row>
                <Row>
                    <Col sm>
                        <span className={"button-border-black"}>
                            <Button className={"banner-btn text-uppercase "}><span>друкуй від 1,5 грн</span></Button>
                        </span>
                    </Col>
                    <Col sm></Col>
                    <Col sm></Col>
                </Row>
            </Container>
        </Container>
    ); 
}

export default FullBaner  
