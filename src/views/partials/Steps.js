import React from 'react';

import FStep from '../../assets/img/ill/step_1.svg';
import SStep from '../../assets/img/ill/step_2.svg';
import TStep from '../../assets/img/ill/step_3.svg';
import FhSteps from '../../assets/img/ill/step_4.svg';
import FvSteps from '../../assets/img/ill/step_5.svg';

import { Container, Col, Row, Image } from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    return (
        <Container className={"mt-5 mb-5"}>
            <Row>
                <Col>
                    <Image src={FStep} alt={'ill_step_1'}/>
                </Col>
                <Col>
                    <Image src={SStep} alt={'ill_step_1'}/>
                </Col>
                <Col>
                    <Image src={TStep} alt={'ill_step_1'}/>
                </Col>
                <Col>
                    <Image src={FhSteps} alt={'ill_step_1'}/>
                </Col>
                <Col>
                    <Image src={FvSteps} alt={'ill_step_1'}/>
                </Col>
            </Row>
            <Row>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>швидко</p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>цілодобово</p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>друк на еко папері</p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>з будь-якого пристрою</p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>з будь-якого мессенджера<br/>пошти чи cloud-сховища</p>
                </Col>
            </Row>
        </Container>
    );
}