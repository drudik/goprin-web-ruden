import React from 'react';

import Phone from '../../assets/img/theme/phone_with_goprint_map.png';
import CQCode from '../../assets/img/theme/barcode_pic.png';
import GooglePlay from '../../assets/img/theme/Google-Play.png';
import AppStore from '../../assets/img/theme/App-Store.png';
import PacMan from '../../assets/img/ill/pac-man-enemy-right-without-eyes.svg';
import MyVerticallyCenteredModal from '../modal/ChoiceApp'

import { Container, Col, Row, Image, Button } from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    const [modalShow, setModalShow] = React.useState(false);
    return (
        <Container className={'section-app pt-5 pb-5'} fluid>
            <Container className={"position-relative"}>
                <Row>
                    <span className={"pac-man-enemy"} width={"90"}>
                        <span className={"eye-orb-l"}>
                            <span className={"y-l"}></span>
                        </span>
                        <span className={"angry-eye"}/>
                        <span className={"eye-orb-r"}>
                            <span className={"y-r"}></span>
                        </span>
                        <img id="eyes" src={PacMan} alt={"pac-man-enemy"} width={"90"}/>
                    </span>
                    <Col className={'s-app-left-side d-flex justify-content-end align-self-center'}>
                        <Image src={Phone} alt={'phone_with_map'}/>
                    </Col>
                    <Col className={'s-app-right-side'}>
                    <Row>
                        <Image className={"qrcode"} src={CQCode} alt={'phone_with_map'}/>
                    </Row>
                    <Row>
                        <Image className={"download-app-img"} src={GooglePlay} alt={'google_paly_store_link'}/>
                        <Image className={"download-app-img"} src={AppStore} alt={'io_app_store_link'}/>
                    </Row>
                    <Row>
                    <span className={"button-border-white"}>
                        <Button className={'section-app_button text-uppercase'} onClick={() => setModalShow(true)}>завантажуй додаток</Button> 
                    </span>
                    
                    <MyVerticallyCenteredModal
                        show={modalShow}
                        onHide={() => setModalShow(false)}
                    />
                    </Row>
                    <Row>
                        <h1 className={'section-app_title text-uppercase mt-2'}>та отримуй 10<br/>безкоштовних сторінок!</h1>
                    </Row>
                    </Col>
                </Row>
            </Container>
        </Container>
    );
}