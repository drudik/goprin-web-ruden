import React from 'react';

import { Container, Row, Col, Button} from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    return (
        <Container className={'action-section-btn cursor-mouse d-flex justify-content-center'} fluid>
            <Row>
                <Col>
                    <span className={"button-border-black"}>
                        <Button className={'action-print-btn cursor-hand-chrome cursor-hand-ffox text-uppercase'}><span>друкувати зараз</span></Button>
                    </span>
                </Col> 
            </Row>
        </Container>
    );
}