import React from 'react';

import FStep from '../../assets/img/ill/faq_step_1.svg';
import SStep from '../../assets/img/ill/faq_step_2.svg';
import TStep from '../../assets/img/ill/faq_step_3.svg';
import FhSteps from '../../assets/img/ill/faq_step_4.svg';
import FvSteps from '../../assets/img/ill/faq_step_5.svg';

import { Container, Col, Row, Image } from 'react-bootstrap';

// eslint-disable-next-line
export default () => {
    return (
        <Container>
            <Row>
                <Col className={"title-faq d-flex justify-content-center text-uppercase mt-5 mb-5  font-roboto-black "}>
                    <h1>Як користуватися GoPrint</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Image src={FStep} alt={'ill_step_1'}/>
                </Col>
                <Col>
                    <Image src={SStep} alt={'ill_step_2'}/>
                </Col>
                <Col>
                    <Image src={TStep} alt={'ill_step_3'}/>
                </Col>
                <Col>
                    <Image src={FhSteps} alt={'ill_step_4'}/>
                </Col>
                <Col>
                    <Image src={FvSteps} alt={'ill_step_5'}/>
                </Col>
            </Row>
            <Row className={"mb-5"}>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>Завантажуй додаток<br/><span className={"p-style"}>або зареєструйся на сайті</span></p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>Завантаж файл<br/><span className={"p-style"}>зі свого смартфона або будь-якого іншого пристрою</span></p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                <p>Обери<br/><span className={"p-style"}>найближчий пункт</span></p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>Сплати<br/><span className={"p-style"}>друк картою</span></p>
                </Col>
                <Col className={"d-flex justify-content-center font-roboto-black title-style-sm text-uppercase"}>
                    <p>Забери<br/><span className={"p-style"}>роздруковані файли</span></p> 

                </Col>
            </Row>
        </Container>
    );
}