import React from 'react';

import Img_1 from '../../assets/img/brand/piramida_logo_partner.png'
import Img_2 from '../../assets/img/brand/velykakyshenia_logo_parnter.png'
import Img_3 from '../../assets/img/brand/skymall_logo_partner.png'

import { Row, Col, Button, Container } from 'react-bootstrap';
import Slider from "react-slick";

// eslint-disable-next-line
export default () => {
    const settings = {
        className: "slider-style",
        centerMode: false,
        infinite: true,
        arrows: false,
        dots: true,
        centerPadding: "60px",
        slidesToShow: 2,
        speed: 500,
        rows: 3,
        slidesPerRow: 1
      };

    return (
        <Container className={"section-partners"} fluid>
            <Row>
                <Col className={"d-flex justify-content-center mt-5 mb-5"}>
                   <h1 className={"title-style font-roboto-black text-uppercase"}>партнерські установи</h1> 
                </Col>
            </Row>
            <Row className={"d-flex justify-content-center"}>
                {/* <Col className={"col-3 d-flex justify-content-start align-self-center"}></Col> */}
                    <Col className={"mb-5 col-lg-4"}>    
                        <Slider {...settings}>
                            {/* col-1 */}
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div> 
                            <div>
                                <img src={Img_3} alt={"partner_skymall"}/>
                            </div>
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div>
                            {/* col-2 */}
                            <div>
                                <img src={Img_2} alt={"partner_velykakyshenia"}/>
                            </div> 
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div>
                            <div>
                                <img src={Img_3} alt={"partner_skymall"}/>
                            </div>
                            {/* col-3 */}
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div> 
                            <div>
                                <img src={Img_3} alt={"partner_skymall"}/>
                            </div>
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div>
                            {/* col-4 */}
                            <div>
                                <img src={Img_2} alt={"partner_velykakyshenia"}/>
                            </div> 
                            <div>
                                <img src={Img_3} alt={"partner_skymall"}/>
                            </div>
                            <div>
                                <img src={Img_1} alt={"partner_piramida"}/>
                            </div>
                        </Slider>
                    </Col>
                {/* <Col className={"col-3 d-flex justify-content-end align-self-center"}></Col> */}
            </Row>
            <Row>
               <Col className={"d-flex justify-content-center"}>
                    <span className={"button-border-black mb-5"}>
                        <Button className={"section-partners-btn text-uppercase"}>стати партнером</Button>
                    </span> 
               </Col> 
            </Row>
        </Container>
    );
}