/* --- Страница для вывода всех скомпилированных страниц.  --- */
// eslint-disable-next-line
import React from 'react';

// import HomePage from './pages/HomePage'
import Test from '../views/Test'

function Compiled(props) {
  return (
    <div>
      {/* <HomePage/> */}
      <Test />
    </div>
  );

}

export default Compiled;
