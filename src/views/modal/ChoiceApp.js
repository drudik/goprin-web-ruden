import React from 'react';

import GooglePlay from '../../assets/img/theme/Google-Play.png';
import AppStore from '../../assets/img/theme/App-Store.png';

import { Modal, Row, Image} from 'react-bootstrap';

function MyVerticallyCenteredModal(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Обери свою платформу
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Row>
                <Image className={"download-app-img"} src={GooglePlay} alt={'google_paly_store_link'}/>
                <Image className={"download-app-img"} src={AppStore} alt={'io_app_store_link'}/>
            </Row>
        </Modal.Body>
        <Modal.Footer>
        </Modal.Footer>
      </Modal>
    );
  }

//   function ChoiceApp() {
//     const [modalShow, setModalShow] = React.useState(false);
  
//     return (
//       <>
//         <Button variant="primary" onClick={() => setModalShow(true)}>
//           Launch vertically centered modal
//         </Button>
  
//         <MyVerticallyCenteredModal
//           show={modalShow}
//           onHide={() => setModalShow(false)}
//         />
//       </>
//     );
//   }
  
  export default MyVerticallyCenteredModal